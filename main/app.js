function differenceOf2Arrays (array1, array2) {
	var temp = [];
  
	for (var i in array1) {
	  if(array2.indexOf(array1[i]) === -1) temp.push(array1[i]);
	}
  
	for(i in array2) {
	  if(array1.indexOf(array2[i]) === -1) temp.push(array2[i]);
	}
	
	return temp.sort((a,b) => a-b);
	}
  
  function distance(first, second){
	  //TODO: implementaÈ›i funcÈ›ia
	  // TODO: implement the function
  
	// if one of the parameters is not an array, return an error
	
	var MyError=function(message){
		var error=new Error(message);
		
		return error;
	}

	  if( typeof first != typeof [] || typeof second != typeof []){
		  throw new MyError("InvalidType");
	  }
  
	  let first_unique_values=[];
	  let second_unique_values=[];
  
	// filter for duplicates
	  for(let i=0; i < first.length; i++){
		  if(first_unique_values.indexOf(first[i])==-1){
			  first_unique_values.push(first[i])
		  }
	  }
  
	// filter for duplicates
	  for(let i=0; i < second.length; i++){
		  if(second_unique_values.indexOf(second[i])==-1){
			  second_unique_values.push(second[i])
		  }
	  }
  
	// return 0 if both arrays are empty
	  if(first_unique_values.length == 0 && second_unique_values.length == 0){
		  return 0;
	  }
  
	// handle the case when one of the arrays can be empty
	 if(first_unique_values.length > 0){
	  var first_array = first_unique_values;
	  var second_array = second_unique_values;
	} else {
	  var first_array = second_unique_values;
	  var second_array = first_unique_values;
	}
  
	// get different elements from arrays
	  var differentElements= differenceOf2Arrays(first_array, second_array);
  
	// return count of different elements
	  return differentElements.length;
	  }
  
  
  module.exports.distance = distance